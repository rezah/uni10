# v2.0 API Changes

## What is changed?

In this release, we made several major changes to make Uni10 more maintainable
and extendable:

1. `Matrix` and `UniTensor` are now  _Class Templates_.

2. Linear algebra operations are no longer methods of `Matrix` and `UniTensor`,
   and are global functions.

3. The coding style now follows [Google C++ Style](https://google.github.io/styleguide/cppguide.html).

4. Uni10 Configuration.

In the following sections, we list the major changes in v2.0 API.

## Changes

###  1. Declaration

####  (a) `Matrix` and `UniTensor` are now  _Class Templates_:

##### `Matrix`

|  Datatype |  Double  | Complex |
|------------|------------|------------|
| v1.0    | `Matrix` | `CMatrix` |
| v2.0    | `Matrix<double>` | `Matrix<std::complex<double>>` |

##### `UniTensor`

|  Datatype |  Double  | Complex |
|------------|------------|------------|
| v1.0    | `UniTensor` | `CUniTensor` |
| v2.0    | `UniTensor<double>` | `UniTensor<std::complex<double>>` |

#### (b) `Network` allows `UniTensor` with different data types. `CNetwork` is depreciated.

|  Datatype |  Double  | Complex |
|--------------|------------|------------|
| v1.0    | `Network` | `CNetwork` |
| v2.0    | `Network` | `Network` |

### 2. Function Names

Following [the function naming rules](https://google.github.io/styleguide/cppguide.html#Function_Names) of Google C++ Style Guide,
1. Ordinary functions should start with a capital letter and have a capital letter for each new word.

2. Accessors and mutators (get and set functions) may be named like variables

#### (a) Member functions of `Block` `b`

| v1.0  |  v2.0 |
| -------------- | ------------------|
| b.isdiag()    | b.diag() |
| b.elemNum()    | b.ElemNum() |
| b.getElem()    | b.GetElem() |
| b.save()       | b.Save()
| b`.at( int, int )`    | b.At( `int`, `int` ) |

#### (b) Member functions of `Matrix` `m`, `cm`

| v1.0  |  v2.0 |
| -------------- | ------------------ |
| m.load( `const std::string&` )    | m.Load( `const std::string&`) |
| m.setElem( `const uni10_double*` ) <br> cm.setElem( `const uni10_complex*`, `bool`) | m.SetElem( `const UniType*`, `bool`) |
| m.setElem( `const std::vector<uni10_double>&` ) <br> cm.setElem( `const std::vector<uni10_comple>&`, `bool`)| m.SetElem( `const std::vector<UniType>&`, `bool`) |

#### (c) Member functions of `UniTensor` `t`

| v1.0  |  v2.0 |
| -------------- |------------------|
| t.assign( )| t.Assign( ) |
| t.at( )| t.At( ) |
| t.blockNum( )| t.BlockNum( ) |
| t.blockQnum( )| t.BlockQnum( ) |

#### (d) Member functions of `Network` `n`

| v1.0  |  v2.0 |
| -------------- | ------------------ |
| n.launch( )    | n.Launch( `UniTensor<UniType>&`) |
| n.putTensor( )    | n.PutTensor() |
| n.putTensorT( )    | n.PutTensorT() |
| NONE   | n.PutTensorD() |
| NONE   | n.PreConstruct() |


### 3. Usage of linear algebra functions

In v2.0, linear algebra functions are no longer member functions of `Block`, `Matrix` and `UniTensor`.
There two types linear-solver functions, `Return Object` and `INPLACE`.

The usage of `Return Object` functions is similar to the previous version, where a copied object is returned.
It consumes more memory.

In this section, we will show how to modify the linear-solver and higher-rank linear-solver functions from v1.0 to v2.0 with `Return Object` functions and please read the v2.0 documentation for the details of `INPLACE` linear-solver functions.

#### (a) `Matrix` operations
| v1.0  |  v2.0 |
| -------------- |------------------|
| m.conj()    |  `Matrix< UniType >` Conj (`const Block< UniType > &` m ) |
| m.dagger()    |  `Matrix< UniType >` Dagger (`const Block< UniType > &` m ) |
| m.transpose()    |  `Matrix< UniType >` Transpose (`const Block< UniType > &` m ) |
| m.inverse()    |  `Matrix< UniType >` Inverse (`const Block< UniType > &` m ) |
| m.sum()    |  `Matrix< UniType >` Sum (`const Block< UniType > &` m ) |
| m.trace()    |  `Matrix< UniType >` Trace (`const Block< UniType > &` m ) |
| m.qr()    |  `Matrix< UniType >` Qr (`const Block< UniType > &` m ) |
| m.lq()    |  `Matrix< UniType >` Lq (`const Block< UniType > &` m ) |
| m.rq()    |  `Matrix< UniType >` Rq (`const Block< UniType > &` m ) |
| m.ql()    |  `Matrix< UniType >` Ql (`const Block< UniType > &` m ) |
| m.norm()   |  `Matrix< UniType >` Norm (`const Block< UniType > &` m ) |
| m.svd()    |  `Matrix< UniType >` Svd (`const Block< UniType > &` m ) |
|            |  `Matrix< UniType >` Sdd (`const Block< UniType > &` m ) |
| m1 * m2    |  `Matrix< UniType >` Dot (`const Block< UniType > &` m1, `const Block< UniType > &` m2 ) |

#### (b) `UniTensor` operations


| v1.0  |  v2.0 |
| -------------- |------------------|
| t.conj()    |  `UniTensor< UniType >` Conj (`const UniTensor< UniType > &` t ) |
| t.dagger()  |  `UniTensor< UniType >` Dagger (`const UniTensor< UniType > &` t ) |
| t.transpose()    | `UniTensor< UniType >` Transpose (`const UniTensor< UniType > &` t ) |
| t.trace()    | `UniTensor< UniType >` Trace (`const UniTensor< UniType > &` t ) |
| t.partialTrace( `int` la, `int` lb )    |  `UniTensor< UniType >` PartialTrace (`const UniTensor< UniType > &` t, `uni10_int` la, `uni10_int` lb ) |
| t.permute( `std::vector<int>& ` newlabels , `int` inbondnum )    |  `UniTensor< UniType >` Permute (`const UniTensor< UniType > &` t,  `std::vector<int>& ` newlabels , `int` inbondnum ) |
| t.permute( `int*` newlabels , `int` inbondnum )    |  `UniTensor< UniType >` Permute (`const UniTensor< UniType > &` t,  `int*` newlabels , `int` inbondnum ) |
| t.permute( `int` inbondnum )    |  `UniTensor< UniType >` Permute (`const UniTensor< UniType > &` t, `int` inbondnum ) |
| `UniTensor` contract( `UniTensor &` t1, `UniTensor &` t2, `bool` fast)    |  `UniTensor< UniType >` Contract (`const UniTensor< UniType > &` t1, `UniTensor< UniType > &` t2 ) |
| `UniTensor` otimes( `UniTensor &` t1, `UniTensor &` t2, `bool` fast)    |  `UniTensor< UniType >` Otimes (`const UniTensor< UniType > &` t1, `UniTensor< UniType > &` t2 ) |

### 4. Uni10 Configuration

To accommodate multiple platforms, it is now possible to customize the runtime
environment of Uni10 Apps. See
 [Uni10 App Development Guide](\ref DevGuide.md) for details.
