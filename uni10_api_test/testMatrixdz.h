#ifndef _TESTMATRIXDZ_H_
#define _TESTMATRIXDZ_H_

#include "gtest/gtest.h"

#include <complex>
#include <random>
#include <vector>

#include "uni10.hpp"

using namespace uni10;

TEST(Matrix, MultipleAssignOperator_notDiagMultinotDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
	std::complex<double> src1[nelem];
	std::complex<double> src2[nelem];
	std::complex<double> ans[nelem];
	std::complex<double> x;
	for (size_t i = 0; i < nelem; ++i) {
	  x.real(i); x.imag(i);
	  src1[i] = x;
          src2[i] = x ;
	  ans[i] = src1[i] * src2[i];
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1);
    Matrix<uni10_complex128> M2(size.row, size.col, src2);
    M1 *= M2;

    for (size_t i = 0; i < nelem; ++i)
      EXPECT_EQ(M1[i], ans[i]);
  }
}

TEST(Matrixdz, OperatorMultiple_notDiagMultinotDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    double src1[nelem];
	std::complex<double> src2[nelem];
	std::complex<double> ans[nelem];
	std::complex<double> x;
    
	for (size_t i = 0; i < nelem; ++i) {
	  x.real(i); x.imag(i);
      src1[i] = (double)i;
      src2[i] = x;
    }

    Matrix<uni10_double64> M1(size.row, size.col, src1);
    Matrix<uni10_complex128> M2(size.row, size.col, src2);
    M2 *= M1;
    for (size_t i = 0; i < nelem; ++i) {
      ans[i] = src1[i] * src2[i];
    }

    for (size_t i = 0; i < nelem; ++i)
      EXPECT_EQ(M2[i], ans[i]);
  }
}

TEST(Matrixdz, OperatorMultiple_notDiagMultiDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    double src1[nelem];
    std::complex<double> src2[std::min(size.row, size.col)];
	std::complex<double> ans[nelem];
	std::complex<double> x;

	for( size_t i = 0; i < std::min(size.row, size.col); ++i){
	  x.real(i); x.imag(i);
	  src2[i] = x;
	}
    for (size_t i = 0; i < nelem; ++i) {
      src1[i] = (double)i;
    }
    
	Matrix<uni10_double64> M1(size.row, size.col, src1);
    Matrix<uni10_complex128> M2(size.row, size.col, src2, true);
    M2 *= M1;
    for (size_t i = 0; i < nelem; ++i) {
	  ans[i] = 0;
    }
	for (size_t i = 0; i < std::min(size.row, size.col) ; ++i){
      ans[i * size.col + i] = src1[i * size.col + i] * src2[i];
	}
    for (size_t i = 0; i < nelem; ++i)
      EXPECT_EQ(M2[i], ans[i]);
  }
}


/*//this test would crash now
TEST(Matrixdz, MultiAssignOperator_DiagMultinotDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    double src1[std::min(size.row, size.col)];
	std::complex<double> src2[nelem];
	std::complex<double> ans[std::min(size.row, size.col)];
	std::complex<double> x;
	for( size_t i = 0; i < std::min(size.row, size.col); ++i){
	  src1[i] = (double)i;
	}
    for (size_t i = 0; i < nelem; ++i) {
	  x.real(i); x.image(i);
      src2[i] = x;
    }
    
	Matrix<uni10_double64> M1(size.row, size.col, src1, true);
    Matrix<uni10_complex128> M2(size.row, size.col, src2);
    M2 *= M1;
	for (size_t i = 0; i < std::min(size.row, size.col); ++i){
      ans[i] = src1[i] * src2[i + i * size.col];
	}
    for (size_t i = 0; i < std::min(size.row, size.col); ++i)
      EXPECT_EQ(M2[i], ans[i]);
  }
}
*/
TEST(Matrixdz, MultipleScalarOperator_notDiagMulti) {

  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    double src1[nelem];
	std::complex<double> ans[nelem];
	std::complex<double> x;
    x.real(1); x.imag(1);
	
	for (size_t i = 0; i < nelem; ++i) {
	  src1[i] = (double)i;
      ans[i] = src1[i] * x;
    }

    Matrix<uni10_double64> M1(size.row, size.col, src1);

    M1 *= x;

    for (size_t i = 0 ; i < nelem; ++i)
      EXPECT_EQ(M1[i], ans[i]);
  }
}

TEST(Matrixdz, MultipleScalarAssignOperator_DiagMulti) {

  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    double src1[std::min(size.row, size.col)];
	std::complex<double> ans[std::min(size.row, size.col)];
	std::complex<double> x;
	x.real(i); x.imag(i);
	
	for (size_t i = 0; i < std::min(size.row, size.col); ++i) {
      src1[i] = (double)i;
	  ans[i] = src1[i] * x;
    }
    Matrix<uni10_double64> M1(size.row, size.col, src1, true);

    M1 *= x;

    for (size_t i = 0 ; i < std::min(size.row, size.col); ++i)
      EXPECT_EQ(M1[i], ans[i]);
  }
}

#endif
