#ifndef _TESTMATRIXZ_H_
#define _TESTMATRIXZ_H_

#include "gtest/gtest.h"

#include <complex>
#include <random>
#include <vector>

#include "uni10.hpp"

using namespace uni10;

TEST(Matrixz, DefaultConstructor) {
  Matrix<uni10_complex128> M;

  EXPECT_EQ(0, M.row());
  EXPECT_EQ(0, M.col());
  EXPECT_FALSE(M.diag());
}

TEST(Matrixz, ConstructorFromSize_notDiag) {
  for (auto size : sizes) {
    Matrix<uni10_complex128> M(size.row, size.col);

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_FALSE(M.diag());
  }
}

TEST(Matrixz, ConstructorFromSize_Diag) {
  for (auto size : sizes) {
    Matrix<uni10_complex128> M(size.row, size.col, true);

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_TRUE(M.diag());
  }
}

TEST(Matrixz, ConstructorFromArray_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];
    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>){(double)i, (double)i};

    Matrix<uni10_complex128> M(size.row, size.col, src);

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_FALSE(M.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(src[i], M[i]);
  }
}

TEST(Matrixz, ConstructorFromArray_Diag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    std::complex<double> src[nelem];
    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>){(double)i, (double)i};

    Matrix<uni10_complex128> M(size.row, size.col, src, true);

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_TRUE(M.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(src[i], M[i]);
  }
}

// TODO add test for constructor which derive elements from a file.
// TEST(Matrixz, Constructor3A)
// TEST(Matrixz, Constructor3B)

// TODO add test for constructor built from Block.
// TEST(Matrixz, Constructor4A)
// TEST(Matrixz, Constructor4B)

TEST(Matrixz, CopyConstructor_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];
    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>){(double)i, (double)i};

    Matrix<uni10_complex128> M1(size.row, size.col, src);
    Matrix<uni10_complex128> M2(M1);

    EXPECT_EQ(M1.row(), M2.row());
    EXPECT_EQ(M1.col(), M2.col());
    EXPECT_EQ(M1.diag(), M2.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], M2[i]);
  }
}

TEST(Matrixz, CopyConstructor_Diag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    std::complex<double> src[nelem];
    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>){(double)i, (double)i};

    Matrix<uni10_complex128> M1(size.row, size.col, src, true);
    Matrix<uni10_complex128> M2(M1);

    EXPECT_EQ(M1.row(), M2.row());
    EXPECT_EQ(M1.col(), M2.col());
    EXPECT_EQ(M1.diag(), M2.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], M2[i]);
  }
}

// TODO add test for destructor
// TEST(Matrixz, Destructor)

TEST(Matrixz, Assign_notDiag) {
  for (auto size : sizes) {
    Matrix<uni10_complex128> M(size.col, size.row);
    // Assign a matrix with another shape
    M.Assign(size.row, size.col);

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_FALSE(M.diag());

    // Assign a square matrix
    M.Assign(size.col, size.col, true);

    EXPECT_EQ(size.col, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_TRUE(M.diag());
  }
}

TEST(Matrixz, Assign_Diag) {
  for (auto size : sizes) {
    Matrix<uni10_complex128> M(size.col, size.row, true);
    // Assign a matrix with another shape
    M.Assign(size.row, size.col);

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_FALSE(M.diag());

    // Assign a square matrix
    M.Assign(size.col, size.row, true);

    EXPECT_EQ(size.col, M.row());
    EXPECT_EQ(size.row, M.col());
    EXPECT_TRUE(M.diag());
  }
}

// TODO add test for load
// TEST(Matrixz, Load)

TEST(Matrixz, SetElemfromArray_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];
    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>){(double)i, (double)i};

    Matrix<uni10_complex128> M(size.row, size.col);
    M.SetElem(src);

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_FALSE(M.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(src[i], M[i]);
  }
}

TEST(Matrixz, SetElemfromArray_Diag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    std::complex<double> src[nelem];
    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>){(double)i, (double)i};

    Matrix<uni10_complex128> M(size.row, size.col, true);
    M.SetElem(src);

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_TRUE(M.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(src[i], M[i]);
  }
}

TEST(Matrixz, SetElemfromVector_notDiag) {
  for (auto size :sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];
    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>){(double)i, (double)i};

    Matrix<uni10_complex128> M(size.row, size.col);
    M.SetElem(src);

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_FALSE(M.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(src[i], M[i]);
  }
}

TEST(Matrixz, SetElemfromVector_Diag) {
  for (auto size : sizes) {
    std::vector<std::complex<double>> src;
    size_t nelem = std::min(size.row, size.col);
    for (double i = 0; i < nelem; ++i)
      src.push_back((std::complex<double>){(double)i, (double)i});

    Matrix<uni10_complex128> M(size.row, size.col, true);
    M.SetElem(src);

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_TRUE(M.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(src[i], M[i]);
  }
}

TEST(Matrixz, SetDiag_notDiag) {
  for (auto size : sizes) {
    Matrix<uni10_complex128> M(size.row, size.col);
    M.SetDiag(true);

    EXPECT_TRUE(M.diag());

    M.SetDiag(false);

    EXPECT_FALSE(M.diag());
  }
}

TEST(Matrixz, SetDiag_Diag) {
  for (auto size : sizes) {
    Matrix<uni10_complex128> M(size.row, size.col, true);
    M.SetDiag(true);

    EXPECT_TRUE(M.diag());

    M.SetDiag(false);

    EXPECT_FALSE(M.diag());
  }
}

TEST(Matrixz, SetZeros_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    Matrix<uni10_complex128> M(size.row, size.col);
    M.Zeros();

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_FALSE(M.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ((std::complex<double>)0, M[i]);
  }
}

TEST(Matrixz, SetZeros_Diag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    Matrix<uni10_complex128> M(size.row, size.col, true);
    M.Zeros();

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_TRUE(M.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ((std::complex<double>)0, M[i]);
  }
}

TEST(Matrixz, Identity_notDiag) {
  for (auto size : sizes) {
    Matrix<uni10_complex128> M(size.row, size.col);
    M.Identity();

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_TRUE(M.diag());
    for (size_t i = 0; i < std::min(size.row, size.col); ++i)
      EXPECT_EQ((std::complex<double>)1, M[i]);
  }
}

TEST(Matrixz, Identity_Diag) {
  for (auto size : sizes) {
    Matrix<uni10_complex128> M(size.row, size.col, true);
    M.Identity();

    EXPECT_EQ(size.row, M.row());
    EXPECT_EQ(size.col, M.col());
    EXPECT_TRUE(M.diag());
    for (size_t i = 0; i < std::min(size.row, size.col); ++i)
      EXPECT_EQ((std::complex<double>)1, M[i]);
  }
}

TEST(Matrixz, AssignOperator_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>){(double)i, (double)i};
    Matrix<uni10_complex128> M1(size.row, size.col, src);

    Matrix<uni10_complex128> M2a;
    M2a = M1;

    EXPECT_EQ(M1.row(), M2a.row());
    EXPECT_EQ(M1.col(), M2a.col());
    EXPECT_EQ(M1.diag(), M2a.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], M2a[i]);

    Matrix<uni10_complex128> M2b(size.col, size.row);
    M2b = M1;

    EXPECT_EQ(M1.row(), M2b.row());
    EXPECT_EQ(M1.col(), M2b.col());
    EXPECT_EQ(M1.diag(), M2b.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], M2b[i]);

    Matrix<uni10_complex128> M2c(size.col, size.col, true);
    M2c = M1;

    EXPECT_EQ(M1.row(), M2c.row());
    EXPECT_EQ(M1.col(), M2c.col());
    EXPECT_EQ(M1.diag(), M2c.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], M2c[i]);
  }
}

TEST(Matrixz, AssignOperator_Diag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>){(double)i, (double)i};
    Matrix<uni10_complex128> M1(size.row, size.col, src, true);

    Matrix<uni10_complex128> M2a;
    M2a = M1;

    EXPECT_EQ(M1.row(), M2a.row());
    EXPECT_EQ(M1.col(), M2a.col());
    EXPECT_EQ(M1.diag(), M2a.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], M2a[i]);

    Matrix<uni10_complex128> M2b(size.col, size.row);
    M2b = M1;

    EXPECT_EQ(M1.row(), M2b.row());
    EXPECT_EQ(M1.col(), M2b.col());
    EXPECT_EQ(M1.diag(), M2b.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], M2b[i]);

    Matrix<uni10_complex128> M2c(size.col, size.col, true);
    M2c = M1;

    EXPECT_EQ(M1.row(), M2c.row());
    EXPECT_EQ(M1.col(), M2c.col());
    EXPECT_EQ(M1.diag(), M2c.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], M2c[i]);
  }
}

TEST(Matrixz, AdditionAssignOperator_notDiagAddnotDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src1[nelem];
    std::complex<double> src2[nelem];
    std::complex<double> ans[nelem];

    for (size_t i = 0; i < nelem; ++i) {
      src1[i] = (std::complex<double>)i;
      src2[i] = (std::complex<double>)i;
      ans[i] = src1[i] + src2[i];
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1);
    Matrix<uni10_complex128> M2(size.row, size.col, src2);
    M1 += M2;

    EXPECT_EQ(size.row, M1.row());
    EXPECT_EQ(size.col, M1.col());
    EXPECT_FALSE(M1.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], ans[i]);
  }
}

/*
TEST(Matrixz, AdditionAssignOperator_notDiagAddDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src1[nelem];
    std::complex<double> src2[std::min(size.row, size.col)];
    std::complex<double> ans[nelem] = {0};

    for (size_t i = 0; i < nelem; ++i) {
      src1[i] = (std::complex<double>)i;
      ans[i] = src1[i];
    }
    for (size_t i = 0; i < std::min(size.row, size.col); ++i) {
      src2[i] = (std::complex<double>)i;
      ans[i * size.col + i] += src2[i];
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1);
    Matrix<uni10_complex128> M2(size.row, size.col, src2, true);
    M1 += M2;

    EXPECT_EQ(size.row, M1.row());
    EXPECT_EQ(size.col, M1.col());
    EXPECT_FALSE(M1.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], ans[i]);
  }
}
*/

// This test cause crashes on current version
/*
   TEST(Matrixz, AdditionAssignOperator_DiagAddnotDiag) {
   for (auto size : sizes) {
   size_t nelem = size.row * size.col;
   std::complex<double> src1[std::min(size.row, size.col)];
   std::complex<double> src2[nelem];
   std::complex<double> ans[nelem] = {0};

   for (size_t i = 0; i < std::min(size.row, size.col); ++i) {
   src1[i] = (std::complex<double>)i;
   ans[i] = src1[i];
   }
   for (size_t i = 0; i < nelem; ++i)
   src2[i] = (std::complex<double>)i;
   for (size_t i = 0; i < std::min(size.row, size.col); ++i)
   ans[i * size.col + i] += src2[i];

   Matrix<uni10_complex128> M1(size.row, size.col, src1, true);
   Matrix<uni10_complex128> M2(size.row, size.col, src2);
   M1 += M2;

   EXPECT_EQ(std::min(size.row, size.col), M1.row());
   EXPECT_EQ(std::min(size.row, size.col), M1.col());
   EXPECT_TRUE(M1.diag());
   for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], ans[i]);
   }
   }
   */

TEST(Matrixz, AdditionAssignOperator_DiagAddDiag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    std::complex<double> src1[nelem];
    std::complex<double> src2[nelem];
    std::complex<double> ans[nelem];

    for (size_t i = 0; i < nelem; ++i) {
      src1[i] = (std::complex<double>)i;
      src2[i] = (std::complex<double>)i;
      ans[i] = src1[i] + src2[i];
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1, true);
    Matrix<uni10_complex128> M2(size.row, size.col, src2, true);
    M1 += M2;

    EXPECT_EQ(size.row, M1.row());
    EXPECT_EQ(size.col, M1.col());
    EXPECT_TRUE(M1.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], ans[i]);
  }
}

TEST(Matrixz, SubtracionAssignOperator_notDiagMinusnotDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src1[nelem];
    std::complex<double> src2[nelem];
    std::complex<double> ans[nelem];

    for (size_t i = 0; i < nelem; ++i) {
      src1[i] = (std::complex<double>)i;
      src2[i] = (std::complex<double>)i;
      ans[i] = src1[i] - src2[i];
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1);
    Matrix<uni10_complex128> M2(size.row, size.col, src2);
    M1 -= M2;

    EXPECT_EQ(size.row, M1.row());
    EXPECT_EQ(size.col, M1.col());
    EXPECT_FALSE(M1.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], ans[i]);
  }
}

/*
   TEST(Matrixz, SubtracionAssignOperator_notDiagMinusDiag) {
   for (auto size : sizes) {
   size_t nelem = size.row * size.col;
   std::complex<double> src1[nelem];
   std::complex<double> src2[std::min(size.row, size.col)];
   std::complex<double> ans[nelem] = {0};

   for (size_t i = 0; i < nelem; ++i) {
   src1[i] = (std::complex<double>)i;
   ans[i] = src1[i];
   }
   for (size_t i = 0; i < std::min(size.row, size.col); ++i) {
   src2[i] = (std::complex<double>)i;
   ans[i * size.col + i] -= src2[i];
   }

   Matrix<uni10_complex128> M1(size.row, size.col, src1);
   Matrix<uni10_complex128> M2(size.row, size.col, src2, true);
   M1 -= M2;

   EXPECT_EQ(size.row, M1.row());
   EXPECT_EQ(size.col, M1.col());
   EXPECT_FALSE(M1.diag());
   for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], ans[i]);
   }
   }
   */

// This test cause crashes on current version
/*
   TEST(Matrixz, SubtracionAssignOperator_DiagMinusnotDiag) {
   for (auto size : sizes) {
   size_t nelem = size.row * size.col;
   std::complex<double> src1[std::min(size.row, size.col)];
   std::complex<double> src2[nelem];
   std::complex<double> ans[nelem] = {0};

   for (size_t i = 0; i < std::min(size.row, size.col); ++i) {
   src1[i] = (std::complex<double>)i;
   ans[i] = src1[i];
   }
   for (size_t i = 0; i < nelem; ++i)
   src2[i] = (std::complex<double>)i;
   for (size_t i = 0; i < std::min(size.row, size.col); ++i)
   ans[i * size.col + i] -= src2[i];

   Matrix<uni10_complex128> M1(size.row, size.col, src1, true);
   Matrix<uni10_complex128> M2(size.row, size.col, src2);
   M1 -= M2;

   EXPECT_EQ(std::min(size.row, size.col), M1.row());
   EXPECT_EQ(std::min(size.row, size.col), M1.col());
   EXPECT_TRUE(M1.diag());
   for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], ans[i]);
   }
   }
   */

TEST(Matrixz, SubtracionAssignOperator_DiagMinusDiag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    std::complex<double> src1[nelem];
    std::complex<double> src2[nelem];
    std::complex<double> ans[nelem];

    for (size_t i = 0; i < nelem; ++i) {
      src1[i] = (std::complex<double>)i;
      src2[i] = (std::complex<double>)i;
      ans[i] = src1[i] - src2[i];
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1, true);
    Matrix<uni10_complex128> M2(size.row, size.col, src2, true);
    M1 -= M2;

    EXPECT_EQ(size.row, M1.row());
    EXPECT_EQ(size.col, M1.col());
    EXPECT_TRUE(M1.diag());
    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M1[i], ans[i]);
  }
}

TEST(Matrixz, SubscriptOperator_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];
    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>)i;

    Matrix<uni10_complex128> M(size.row, size.col, src);

    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M[i], src[i]);
  }
}

TEST(Matrixz, SubscriptOperator_Diag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    std::complex<double> src[nelem];
    for (size_t i = 0; i < nelem; ++i)
      src[i] = (std::complex<double>)i;

    Matrix<uni10_complex128> M(size.row, size.col, src, true);

    for (size_t i = 0; i < nelem; ++i) EXPECT_EQ(M[i], src[i]);
  }
}

TEST(Matrix, MultiplicationAssignOperator_notDiagMultinotDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src1[nelem];
    std::complex<double> src2[nelem];
    std::complex<double> ans[nelem];
    std::complex<double> x;
    for (size_t i = 0; i < nelem; ++i) {
      x.real(i); x.imag(i);
      src1[i] = x;
      src2[i] = x ;
      ans[i] = src1[i] * src2[i];
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1);
    Matrix<uni10_complex128> M2(size.row, size.col, src2);
    M1 *= M2;

    for (size_t i = 0; i < nelem; ++i)
      EXPECT_EQ(M1[i], ans[i]);
  }
}

/*
   TEST(Matrix, MultiplicationAssignOperator_notDiagMultiDiag) {
   for (auto size : sizes) {
   size_t nelem = size.row * size.col;
   std::complex<double> src1[nelem];
   std::complex<double>  src2[std::min(size.row, size.col)];
   std::complex<double>  ans[nelem];
   std::complex<double> x;
   for (size_t i = 0; i < nelem; ++i) {
   x.real(i); x.imag(i);
   src1[i] = x;
   ans[i] = 0;
   }

   for( size_t i = 0; i < std::min(size.row, size.col); ++i){
   src2[i] = (std::complex<double>)i;
   ans[i * size.col + i] = src1[i * size.col + i] * src2[i];
   }

   Matrix<uni10_complex128> M1(size.row, size.col, src1);
   Matrix<uni10_complex128> M2(size.row, size.col, src2, true);
   M1 *= M2;

   for (size_t i = 0; i < nelem; ++i)
   EXPECT_EQ(M1[i], ans[i]);
   }
   }
   */

//this test could cause crash
/*TEST(Matrix, MultiplicationAssignOperator_DiagMultinotDiag) {
  for (auto size : sizes) {
  size_t nelem = size.row * size.col;
  std::complex<double> src1[std::min(size.row, size.col)];
  std::complex<double> src2[nelem];
  std::complex<double> ans[std::min(size.row, size.col)];
  std::complex<double> x;
  for( size_t i = 0; i < std::min(size.row, size.col); ++i){
  x.real(i); x.imag(i);
  src1[i] = x;
  }
  for (size_t i = 0; i < nelem; ++i) {
  x.real(i); x.imag(i);
  src2[i] = x;
  }

  Matrix<uni10_complex128> M1(size.row, size.col, src1, true);
  Matrix<uni10_complex128> M2(size.row, size.col, src2);
  M1 *= M2;

  for (size_t i = 0; i < std::min(size.row, size.col); ++i){
  ans[i] = src1[i] * src2[i + i * size.col];
  }
  for (size_t i = 0; i < std::min(size.row, size.col); ++i)
  EXPECT_EQ(M1[i], ans[i]);
  }
  }
  */
TEST(Matrix, MultiplicationAssignOperator_scalar_notDiag) {

  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src1[nelem];
    std::complex<double> ans[nelem];
    std::complex<double> x;

    for (size_t i = 0; i < nelem; ++i) {
      x.real(i); x.imag(i);
      src1[i] = x;
      ans[i] = src1[i] * (std::complex<double>)2;
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1);

    M1 *= 2;

    for (size_t i = 0 ; i < nelem; ++i)
      EXPECT_EQ(M1[i], ans[i]);
  }
}

TEST(Matrix, MultiplicationAssignOperator_scalar_Diag) {

  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src1[std::min(size.row, size.col)];
    std::complex<double> ans[std::min(size.row, size.col)];
    std::complex<double> x;

    for (size_t i = 0; i < std::min(size.row, size.col); ++i) {
      x.real(i); x.imag(i);
      src1[i] = x;
      ans[i] = src1[i] * (std::complex<double>)2;
    }
    Matrix<uni10_complex128> M1(size.row, size.col, src1, true);

    M1 *= 2;

    for (size_t i = 0 ; i < std::min(size.row, size.col); ++i)
      EXPECT_EQ(M1[i], ans[i]);
  }
}
/*
   TEST(Matrix, MultipleOperator_notDiagMultinotDiag) {
   for (auto size : sizes) {
   size_t nelem = size.row * size.col;
   std::complex<double> src1[nelem];
   std::complex<double> src2[nelem];
   std::complex<double> src3[nelem];
   std::complex<double> ans[nelem];

   for (size_t i = 0; i < nelem; ++i) {
   src1[i] = (std::complex<double>)i ;
   src2[i] = (std::complex<double>)i ;
   ans[i] = src1[i] * src2[i];
   }

   Matrix<uni10_complex128> M1(size.row, size.col, src1);
   Matrix<uni10_complex128> M2(size.row, size.col, src2);
   Matrix<uni10_complex128> M3(size.row, size.col, src3);
   M3 = M1 * M2;

   for (size_t i = 0; i < nelem; ++i)
   EXPECT_EQ(M3[i], ans[i]);
   }
   }

   TEST(Matrix,MultipleOperator_notDiagMultiDiag) {
   for (auto size : sizes) {
   size_t nelem = size.row * size.col;
   std::complex<double> src1[nelem];
   std::complex<double>  src2[std::min(size.row, size.col)];
   std::complex<double>  src3[nelem];
   std::complex<double>  ans[nelem];

   for (size_t i = 0; i < nelem; ++i) {
   src1[i] = (std::complex<double>)i;
   ans[i] = 0;
   }

   for( size_t i = 0; i < std::min(size.row, size.col); ++i){
   src2[i] = (std::complex<double>)i;
   ans[i * size.col + i] = src1[i * size.col + i] * src2[i];
   }

   Matrix<uni10_complex128> M1(size.row, size.col, src1);
   Matrix<uni10_complex128> M2(size.row, size.col, src2, true);
   Matrix<uni10_complex128> M3(size.row, size.col, src3);
   M3 = M1 * M2;

   for (size_t i = 0; i < nelem; ++i)
   EXPECT_EQ(M3[i], ans[i]);
   }
   }


//this test could cause crash
TEST(Matrixz, MultipleOperator_DiagMultinotDiag) {
for (auto size : sizes) {
size_t nelem = size.row * size.col;
std::complex<double> src1[std::min(size.row, size.col)];
std::complex<double> src2[nelem];
std::complex<double> ans[std::min(size.row, size.col)];
std::complex<double> src3[std::min(size.row, size.col)];

for( size_t i = 0; i < std::min(size.row, size.col); ++i){
src1[i] = (std::complex<double>)i;
}
for (size_t i = 0; i < nelem; ++i) {
src2[i] = (std::complex<double>)i;
}

Matrix<uni10_complex128> M1(size.row, size.col, src1, true);
Matrix<uni10_complex128> M2(size.row, size.col, src2);
Matrix<uni10_complex128> M3(size.row, size.col, src3, true);
M3 = M1 * M2;

for (size_t i = 0; i < std::min(size.row, size.col); ++i){
  ans[i] = src1[i] * src2[i + i * size.col];
}
for (size_t i = 0; i < std::min(size.row, size.col); ++i)
EXPECT_EQ(M3[i], ans[i]);
}
}


TEST(Matrixz, MultipleScalarOperator_notDiagMulti) {

  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src1[nelem];
    std::complex<double> src2[nelem];
    std::complex<double> ans[nelem];

    for (size_t i = 0; i < nelem; ++i) {
      src1[i] = (std::complex<double>)i;
      ans[i] = src1[i] * (std::complex<double>)2;
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1);
    Matrix<uni10_complex128> M2(size.row, size.col, src2);

    M2 = M1 * 2;

    for (size_t i = 0 ; i < nelem; ++i)
      EXPECT_EQ(M2[i], ans[i]);
  }
}

TEST(Matrixz, MultipleScalarOperator_DiagMulti) {

  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src1[std::min(size.row, size.col)];
    std::complex<double> src2[std::min(size.row, size.col)];
    std::complex<double> ans[std::min(size.row, size.col)];

    for (size_t i = 0; i < std::min(size.row, size.col); ++i) {
      src1[i] = (std::complex<double>)i;
      ans[i] = src1[i] * (std::complex<double>)2;
    }
    Matrix<uni10_complex128> M1(size.row, size.col, src1, true);
    Matrix<uni10_complex128> M2(size.row, size.col, src1, true);

    M2 = M1 * 2;

    for (size_t i = 0 ; i < std::min(size.row, size.col); ++i)
      EXPECT_EQ(M2[i], ans[i]);
  }
}
*/
TEST(Matrixz, DotAssignInplace_notDiagDotnotDiag) {
  for (auto size: sizes){
    size_t nelem = size.row * size.col;
    std::complex<double> src1[nelem];
    std::complex<double> src2[nelem];
    std::complex<double> ans[size.row * size.row];
    std::complex<double> x;

    for (size_t i = 0 ; i < nelem; ++i) {
      x.real(i); x.imag(i);
      src1[i] = x;
      src2[i] = x;
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1);
    Matrix<uni10_complex128> M2(size.col, size.row, src2);
    Matrix<uni10_complex128> M3(size.row, size.row);

    for (size_t i = 0; i < size.row * size.row; ++i) {
      ans[i] = 0;
      int k1 = i / size.row;
      int k2 = i % size.row;
      for (size_t j = 0; j < size.col; ++j) {
        ans[i] += src1[k1 * size.col + j] * src2[k2 + size.row * j];
      }
    }

    Dot(M3, M1, M2, INPLACE);

    for (size_t i = 0 ; i < size.row * size.row; ++i)
      EXPECT_EQ(M3[i], ans[i]);
  }
}
/*
   TEST(Matrixz, DotInplace_DiagDotnotDiag) {
   for (auto size: sizes){
   size_t nelem = size.row * size.col;
   std::complex<double> src1[std::min(size.row, size.col)];
   std::complex<double> src2[nelem];
   std::complex<double> ans[size.row * size.row];
   std::complex<double> x;

   for (size_t i = 0 ; i < std::min(size.row, size.col); ++i) {
   x.real(i); x.imag(i);
   src1[i] = x;
   }
   for (size_t i = 0 ; i < nelem; ++i) {
   x.real(i); x.imag(i);
   src2[i] = x;
   }

   Matrix<uni10_complex128> M1(size.row, size.col, src1, true);
   Matrix<uni10_complex128> M2(size.col, size.row, src2);

   for (size_t i = 0; i < size.row * size.row; ++i) {
   ans[i] = 0;
   int k1 = i / size.row;
   int k2 = i % size.row;
   ans[i] += src1[k1] * src2[k2 * size.row + k2];
   }

   Dot(M1, M2, INPLACE);

   for (size_t i = 0 ; i < size.row * size.row; ++i)
   EXPECT_EQ(M1[i], ans[i]);
   }
   }
   */

/*
   TEST(Matrixz, DotInplace_notDiagDotDiag) {
   for (auto size: sizes){
   size_t nelem = size.row * size.col;
   std::complex<double> src1[nelem];
   std::complex<double> src2[std::min(size.row, size.col)];
   std::complex<double> ans[size.row * size.row];
   std::complex<double> x;


   for (size_t i = 0 ; i < nelem; ++i) {
   x.real(i); x.imag(i);
   src1[i] = x;
   }
   for (size_t i = 0 ; i < std::min(size.row, size.col); ++i) {
   x.real(i); x.imag(i);
   src2[i] = x;
   }

   Matrix<uni10_complex128> M1(size.row, size.col, src1);
   Matrix<uni10_complex128> M2(size.col, size.row, src2, true);

   for (size_t i = 0; i < size.row * size.row; ++i) {
   ans[i] = 0;
   int k1 = i / size.row;
   int k2 = i % size.row;
   ans[i] += src1[k1 * size.col + k1] * src2[k2];
   }

   Dot(M1, M2, INPLACE);

   for (size_t i = 0 ; i < size.row * size.row; ++i)
   EXPECT_EQ(M1[i], ans[i]);
   }
   }
   */

TEST(Matrixz, DotInplace_DiagDotDiag) {
  for (auto size: sizes){
    std::complex<double> src1[std::min(size.row, size.col)];
    std::complex<double> src2[std::min(size.row, size.col)];
    std::complex<double> ans[std::min(size.row, size.col)];
    std::complex<double> x;

    for (size_t i = 0 ; i < std::min(size.row, size.col); ++i) {
      x.real(i); x.imag(i);
      src1[i] = x;
      src2[i] = x;
    }

    Matrix<uni10_complex128> M1(size.row, size.col, src1, true);
    Matrix<uni10_complex128> M2(size.col, size.row, src2, true);

    for (size_t i = 0; i < std::min(size.row, size.col); ++i) {
      ans[i] = 0;
      ans[i] += src1[i] * src2[i];
    }

    Dot(M1, M2, INPLACE);

    for (size_t i = 0 ; i < std::min(size.row, size.col); ++i)
      EXPECT_EQ(M1[i], ans[i]);
  }
}

TEST(Matrixz, QR_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row >= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i,i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.col, size.col);
      auto QR = Qr(M);
      Id.Identity();

      // Check if M equals to Q * R
      double eps1 = Norm(M - Dot(QR.at(0), QR.at(1)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(Dagger(QR.at(0)), QR.at(0)));

      // Check if R is upper triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < QR.at(1).row() && isCorrect; ++i) {
        for (size_t j = 0; j < i && isCorrect; ++j) {
          if (QR.at(1)[i * QR.at(1).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(QR.at(0).row(), size.row);
      EXPECT_EQ(QR.at(0).col(), size.col);
      EXPECT_EQ(QR.at(1).row(), size.col);
      EXPECT_EQ(QR.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, RQ_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row <= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i,i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.row, size.row);
      auto RQ = Rq(M);
      Id.Identity();

      // Check if M equals to R * Q
      double eps1 = Norm(M - Dot(RQ.at(0), RQ.at(1)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(RQ.at(1), Dagger(RQ.at(1))));

      // Check if R is upper triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < RQ.at(0).row() && isCorrect; ++i) {
        for (size_t j = 0; j < i && isCorrect; ++j) {
          if (RQ.at(0)[i * RQ.at(0).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(RQ.at(0).row(), size.row);
      EXPECT_EQ(RQ.at(0).col(), size.row);
      EXPECT_EQ(RQ.at(1).row(), size.row);
      EXPECT_EQ(RQ.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, QL_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row >= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i,i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.col, size.col);
      auto QL = Ql(M);
      Id.Identity();

      // Check if M equals to Q * L
      double eps1 = Norm(M - Dot(QL.at(0), QL.at(1)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(Dagger(QL.at(0)), QL.at(0)));

      // Check if L is lower triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < QL.at(1).row() && isCorrect; ++i) {
        for (size_t j = i + 1; j < QL.at(1).col() && isCorrect; ++j) {
          if (QL.at(1)[i * QL.at(1).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(QL.at(0).row(), size.row);
      EXPECT_EQ(QL.at(0).col(), size.col);
      EXPECT_EQ(QL.at(1).row(), size.col);
      EXPECT_EQ(QL.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, LQ_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row <= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i,i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.row, size.row);
      auto LQ = Lq(M);
      Id.Identity();

      // Check if M equals to L * Q
      double eps1 = Norm(M - Dot(LQ.at(0), LQ.at(1)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(LQ.at(1), Dagger(LQ.at(1))));

      // Check if L is lower triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < LQ.at(0).row() && isCorrect; ++i) {
        for (size_t j = i + 1; j < LQ.at(0).col() && isCorrect; ++j) {
          if (LQ.at(0)[i * LQ.at(0).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(LQ.at(0).row(), size.row);
      EXPECT_EQ(LQ.at(0).col(), size.row);
      EXPECT_EQ(LQ.at(1).row(), size.row);
      EXPECT_EQ(LQ.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, QDR_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row >= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i,i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.col, size.col);
      auto QDR = Qdr(M);
      Id.Identity();

      // Check if M equals to Q * D * R
      double eps1 = Norm(M - Dot(Dot(QDR.at(0), QDR.at(1)), QDR.at(2)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(Dagger(QDR.at(0)), QDR.at(0)));

      // Check if R is upper triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < QDR.at(2).row() && isCorrect; ++i) {
        for (size_t j = 0; j < i && isCorrect; ++j) {
          if (QDR.at(2)[i * QDR.at(2).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(QDR.at(0).row(), size.row);
      EXPECT_EQ(QDR.at(0).col(), size.col);
      EXPECT_EQ(QDR.at(1).row(), size.col);
      EXPECT_EQ(QDR.at(1).col(), size.col);
      EXPECT_TRUE(QDR.at(1).diag());
      EXPECT_EQ(QDR.at(2).row(), size.col);
      EXPECT_EQ(QDR.at(2).col(), size.col);
    }
  }
}

TEST(Matrixz, LDQ_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row <= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i,i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.row, size.row);
      auto LDQ = Ldq(M);
      Id.Identity();

      // Check if M equals to L * D *Q
      double eps1 = Norm(M - Dot(Dot(LDQ.at(0), LDQ.at(1)), LDQ.at(2)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(LDQ.at(2), Dagger(LDQ.at(2))));

      // Check if L is lower triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < LDQ.at(0).row() && isCorrect; ++i) {
        for (size_t j = i + 1; j < LDQ.at(0).col() && isCorrect; ++j) {
          if (LDQ.at(0)[i * LDQ.at(0).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(LDQ.at(0).row(), size.row);
      EXPECT_EQ(LDQ.at(0).col(), size.row);
      EXPECT_EQ(LDQ.at(1).row(), size.row);
      EXPECT_EQ(LDQ.at(1).col(), size.row);
      EXPECT_TRUE(LDQ.at(1).diag());
      EXPECT_EQ(LDQ.at(2).row(), size.row);
      EXPECT_EQ(LDQ.at(2).col(), size.col);
    }
  }
}

TEST(Matrixz, QDR_cpivot_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row == size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i,i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.col, size.col);
      auto QDR = QdrColPivot(M);
      Id.Identity();

      // Check if M equals to Q * D * R
      double eps1 = Norm(M - Dot(Dot(QDR.at(0), QDR.at(1)), QDR.at(2)));

      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(Dagger(QDR.at(0)), QDR.at(0)));

      // Check if D is Diagnal and the singular value is decreasing
      bool isDCorrect = true;
      for (decltype(QDR.at(1).row()) i = 1;
          i < std::min(QDR.at(1).row(), QDR.at(1).col()); ++i)
        if (abs(QDR.at(1)[i]) > abs(QDR.at(1)[i - 1])) isDCorrect = false;

      // Check if R is upper triangular matrix
      bool isRCorrect = true;
      std::vector<bool> zeroCnt(QDR.at(2).col() + 1, false);
      for (decltype(QDR.at(2).col()) i = 0;
          i < QDR.at(2).col() && isRCorrect; ++i) {
        int count = 0;
        auto elem_ind = i;
        while (count < QDR.at(2).row() && QDR.at(2)[elem_ind] != 0.0) {
          elem_ind += QDR.at(2).col();
          count++;
        }
        while (count < QDR.at(2).row() && QDR.at(2)[elem_ind] == 0.0) {
          elem_ind += QDR.at(2).col();
          count++;
        }
        if (count != QDR.at(2).row()) isRCorrect = false;
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isDCorrect);
      EXPECT_TRUE(isRCorrect);
      EXPECT_EQ(QDR.at(0).row(), size.row);
      EXPECT_EQ(QDR.at(0).col(), size.col);
      EXPECT_EQ(QDR.at(1).row(), size.col);
      EXPECT_EQ(QDR.at(1).col(), size.col);
      EXPECT_TRUE(QDR.at(1).diag());
      EXPECT_EQ(QDR.at(2).row(), size.col);
      EXPECT_EQ(QDR.at(2).col(), size.col);
    }
  }
}

TEST(Matrixz, SVD_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    size_t sizeMin   = std::min(size.row, size.col);
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i)
      src[i] = std::complex<double> (i,i);

    Matrix<uni10_complex128> M(size.row, size.col, src);
    Matrix<uni10_complex128> Id(sizeMin, sizeMin);
    auto SVD = Svd(M);
    Id.Identity();

    // Check if M equals to U * S * V^T
    double eps1 = Norm(M - Dot(Dot(SVD.at(0), SVD.at(1)), SVD.at(2)));
    // Check U's orthogonality
    double eps2 = SVD.at(0).row() < SVD.at(0).col() ?
      Norm(Id - Dot(SVD.at(0), Dagger(SVD.at(0)))):
      Norm(Id - Dot(Dagger(SVD.at(0)), SVD.at(0)));
    // Check V's orthogonality
    double eps3 = SVD.at(2).row() < SVD.at(2).col() ?
      Norm(Id - Dot(SVD.at(2), Dagger(SVD.at(2)))):
      Norm(Id - Dot(Dagger(SVD.at(2)), SVD.at(2)));

    EXPECT_LE(eps1, error);
    EXPECT_LE(eps2, error);
    EXPECT_LE(eps3, error);
    EXPECT_EQ(SVD.at(0).row(), size.row);
    EXPECT_EQ(SVD.at(0).col(), sizeMin);
    EXPECT_EQ(SVD.at(1).row(), sizeMin);
    EXPECT_EQ(SVD.at(1).col(), sizeMin);
    EXPECT_TRUE(SVD.at(1).diag());
    EXPECT_EQ(SVD.at(2).row(), sizeMin);
    EXPECT_EQ(SVD.at(2).col(), size.col);
  }
}

TEST(Matrixz, SDD_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    size_t sizeMin   = std::min(size.row, size.col);
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i)
      src[i] = std::complex<double> (i,i);

    Matrix<uni10_complex128> M(size.row, size.col, src);
    Matrix<uni10_complex128> Id(sizeMin, sizeMin);
    auto SDD = Sdd(M);
    Id.Identity();

    // Check if M equals to U * S * V^T
    double eps1 = Norm(M - Dot(Dot(SDD.at(0), SDD.at(1)), SDD.at(2)));
    // Check U's orthogonality
    double eps2 = SDD.at(0).row() < SDD.at(0).col() ?
      Norm(Id - Dot(SDD.at(0), Dagger(SDD.at(0)))):
      Norm(Id - Dot(Dagger(SDD.at(0)), SDD.at(0)));
    // Check V's orthogonality
    double eps3 = SDD.at(2).row() < SDD.at(2).col() ?
      Norm(Id - Dot(SDD.at(2), Dagger(SDD.at(2)))):
      Norm(Id - Dot(Dagger(SDD.at(2)), SDD.at(2)));

    EXPECT_LE(eps1, error);
    EXPECT_LE(eps2, error);
    EXPECT_LE(eps3, error);
    EXPECT_EQ(SDD.at(0).row(), size.row);
    EXPECT_EQ(SDD.at(0).col(), sizeMin);
    EXPECT_EQ(SDD.at(1).row(), sizeMin);
    EXPECT_EQ(SDD.at(1).col(), sizeMin);
    EXPECT_TRUE(SDD.at(1).diag());
    EXPECT_EQ(SDD.at(2).row(), sizeMin);
    EXPECT_EQ(SDD.at(2).col(), size.col);
  }
}

TEST(Matrixz, Eigh_notDiag) {
  for (auto size : sizes) {
    if (size.row == size.col) {
      size_t nelem = size.row * size.col;
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double>(i, i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Mc = Dagger(M);

      for (size_t i = 0; i < size.row; ++i) {
        for (size_t j = i + 1; j < size.col; ++j) {
          M[i * size.col + j] = Mc[i * size.col + j];
        }
      }
      auto EIGH = EigH(M);

      // Check if M equals to U * S * V^T
      //double eps = Norm(M - Dot(Dot(Dagger(EIGH.at(1)), EIGH.at(0)), EIGH.at(1)));

      //EXPECT_LE(eps, error);
      EXPECT_EQ(EIGH.at(0).row(), size.row);
      EXPECT_EQ(EIGH.at(0).col(), size.col);
      EXPECT_TRUE(EIGH.at(0).diag());
      EXPECT_EQ(EIGH.at(1).row(), size.row);
      EXPECT_EQ(EIGH.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, Eig_notDiag) {
  for (auto size : sizes) {
    if (size.row == size.col) {
      size_t nelem = size.row * size.col;
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double>(i, i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      auto EIG = Eig(M);

      // Check if M equals to U * S * V^T
      double eps = Norm(M - Dot(Dot(Inverse(EIG.at(1)), EIG.at(0)), EIG.at(1)));

      EXPECT_LE(eps, error);
      EXPECT_EQ(EIG.at(0).row(), size.row);
      EXPECT_EQ(EIG.at(0).col(), size.col);
      EXPECT_TRUE(EIG.at(0).diag());
      EXPECT_EQ(EIG.at(1).row(), size.row);
      EXPECT_EQ(EIG.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, Sum_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];
    std::complex<double> ans(0, 0);

    for (size_t i = 0; i < nelem; ++i) {
      src[i] = std::complex<double> (i, i);
      ans += std::complex<double> (i, i);
    }

    Matrix<uni10_complex128> M(size.row, size.col, src);
    std::complex<double> SUM = Sum(M);

    EXPECT_LE(std::abs(SUM - ans), error);
  }
}

TEST(Matrixz, Sum_Diag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    std::complex<double> src[nelem];
    std::complex<double> ans(0, 0);

    for (size_t i = 0; i < nelem; ++i) {
      src[i] = std::complex<double> (i, i);
      ans += std::complex<double> (i, i);
    }

    Matrix<uni10_complex128> M(size.row, size.col, src, true);
    std::complex<double> SUM = Sum(M);

    EXPECT_LE(abs(SUM-ans), error);
  }
}

TEST(Matrixz, Norm_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i) {
      src[i] = std::complex<double> (i, i);
    }

    Matrix<uni10_complex128> M(size.row, size.col, src);
    std::complex<double> N = Norm(M);
    std::complex<double> Ans = sqrt(Trace(Dot(Dagger(M), M)));

    EXPECT_LE(abs(N-Ans), error);
  }
}

TEST(Matrixz, Norm_Diag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    std::complex<double> src[nelem];
    std::complex<double> ans(0, 0);

    for (size_t i = 0; i < nelem; ++i) {
      src[i] = std::complex<double> (i, i);
      ans += norm(std::complex<double> (i, i));
    }

    Matrix<uni10_complex128> M(size.row, size.col, src, true);
    std::complex<double> N = Norm(M);

    EXPECT_LE(abs(N-sqrt(ans)), error);
  }
}

TEST(Matrixz, Transpose_notDiag) {
  for (auto size : sizes){
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i) {
      src[i] = std::complex<double> (i, i);
    }

    Matrix<uni10_complex128> M(size.row, size.col, src);

    auto T = Transpose(M);

    EXPECT_EQ(T.row(), size.col);
    EXPECT_EQ(T.col(), size.row);
    for (size_t i = 0 ; i < size.col; ++i){
      for (size_t j = 0 ; j < size.row; ++j){
        EXPECT_EQ(T[i * size.row + j], M[i + j * size.col]);
      }
    }
  }
}

TEST(Matrixz, Dagger_notDiag) {
  for (auto size : sizes){
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i) {
      src[i] = std::complex<double> (i, i);
    }

    Matrix<uni10_complex128> M(size.row, size.col, src);

    auto D = Dagger(M);

    EXPECT_EQ(D.row(), size.col);
    EXPECT_EQ(D.col(), size.row);
    for (size_t i = 0 ; i < size.col; ++i){
      for (size_t j = 0 ; j < size.row; ++j){
        EXPECT_EQ(D[i * size.row + j], std::conj(M[i + j * size.col]));
      }
    }
  }
}

TEST(Matrixz, Conj_notDiag) {
  for (auto size : sizes){
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i) {
      src[i] = std::complex<double> (i, i);
    }

    Matrix<uni10_complex128> M(size.row, size.col, src);

    auto C = Conj(M);

    EXPECT_EQ(C.row(), size.row);
    EXPECT_EQ(C.col(), size.col);
    for (size_t i = 0 ; i < nelem; ++i){
      EXPECT_EQ(C[i], std::conj(M[i]));
    }
  }
}

TEST(Matrixz, Trace_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];
    std::complex<double> ans(0, 0);

    for (size_t i = 0; i < nelem; ++i)
      src[i] = std::complex<double>(i, i);
    for (size_t i = 0; i < size.row && i < size.col; ++i)
      ans += src[i * size.col + i];

    Matrix<uni10_complex128> M(size.row, size.col, src);
    std::complex<double> TRACE = Trace(M);

    EXPECT_LE(abs(TRACE-ans), error);
  }
}

TEST(Matrixz, Trace_Diag) {
  for (auto size : sizes) {
    size_t nelem = std::min(size.row, size.col);
    std::complex<double> src[nelem];
    std::complex<double> ans(0, 0);

    for (size_t i = 0; i < nelem; ++i) {
      src[i] = std::complex<double>(i, i);
      ans += src[i];
    }

    Matrix<uni10_complex128> M(size.row, size.col, src, true);
    std::complex<double> TRACE = Trace(M);

    EXPECT_LE(abs(TRACE-ans), error);
  }
}

TEST(Matrixz, QR_inplace_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row >= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i, i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.col, size.col);
      Id.Identity();
      std::vector<Matrix<uni10_complex128>> QR(2);
      Qr(M, QR.at(0), QR.at(1), INPLACE);

      // Check if M equals to Q * R
      double eps1 = Norm(M - Dot(QR.at(0), QR.at(1)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(Dagger(QR.at(0)), QR.at(0)));

      // Check if R is upper triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < QR.at(1).row() && isCorrect; ++i) {
        for (size_t j = 0; j < i && isCorrect; ++j) {
          if (QR.at(1)[i * QR.at(1).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(QR.at(0).row(), size.row);
      EXPECT_EQ(QR.at(0).col(), size.col);
      EXPECT_EQ(QR.at(1).row(), size.col);
      EXPECT_EQ(QR.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, RQ_inplace_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row <= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i, i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.row, size.row);
      Id.Identity();
      std::vector<Matrix<uni10_complex128>> RQ(2);
      Rq(M, RQ.at(0), RQ.at(1), INPLACE);

      // Check if M equals to R * Q
      double eps1 = Norm(M - Dot(RQ.at(0), RQ.at(1)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(RQ.at(1), Dagger(RQ.at(1))));

      // Check if R is upper triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < RQ.at(0).row() && isCorrect; ++i) {
        for (size_t j = 0; j < i && isCorrect; ++j) {
          if (RQ.at(0)[i * RQ.at(0).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(RQ.at(0).row(), size.row);
      EXPECT_EQ(RQ.at(0).col(), size.row);
      EXPECT_EQ(RQ.at(1).row(), size.row);
      EXPECT_EQ(RQ.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, QL_inplace_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row >= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i, i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.col, size.col);
      Id.Identity();
      std::vector<Matrix<uni10_complex128>> QL(2);
      Ql(M, QL.at(0), QL.at(1), INPLACE);

      // Check if M equals to Q * L
      double eps1 = Norm(M - Dot(QL.at(0), QL.at(1)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(Dagger(QL.at(0)), QL.at(0)));

      // Check if L is lower triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < QL.at(1).row() && isCorrect; ++i) {
        for (size_t j = i + 1; j < QL.at(1).col() && isCorrect; ++j) {
          if (QL.at(1)[i * QL.at(1).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(QL.at(0).row(), size.row);
      EXPECT_EQ(QL.at(0).col(), size.col);
      EXPECT_EQ(QL.at(1).row(), size.col);
      EXPECT_EQ(QL.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, LQ_inplace_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row <= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i, i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.row, size.row);
      Id.Identity();
      std::vector<Matrix<uni10_complex128>> LQ(2);
      Lq(M, LQ.at(0), LQ.at(1), INPLACE);

      // Check if M equals to L * Q
      double eps1 = Norm(M - Dot(LQ.at(0), LQ.at(1)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(LQ.at(1), Dagger(LQ.at(1))));

      // Check if L is lower triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < LQ.at(0).row() && isCorrect; ++i) {
        for (size_t j = i + 1; j < LQ.at(0).col() && isCorrect; ++j) {
          if (LQ.at(0)[i * LQ.at(0).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(LQ.at(0).row(), size.row);
      EXPECT_EQ(LQ.at(0).col(), size.row);
      EXPECT_EQ(LQ.at(1).row(), size.row);
      EXPECT_EQ(LQ.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, QDR_inplace_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row >= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i, i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.col, size.col);
      Id.Identity();
      std::vector<Matrix<uni10_complex128>> QDR(3);
      Qdr(M, QDR.at(0), QDR.at(1), QDR.at(2), INPLACE);

      // Check if M equals to Q * D * R
      double eps1 = Norm(M - Dot(Dot(QDR.at(0), QDR.at(1)), QDR.at(2)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(Dagger(QDR.at(0)), QDR.at(0)));

      // Check if R is upper triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < QDR.at(2).row() && isCorrect; ++i) {
        for (size_t j = 0; j < i && isCorrect; ++j) {
          if (QDR.at(2)[i * QDR.at(2).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(QDR.at(0).row(), size.row);
      EXPECT_EQ(QDR.at(0).col(), size.col);
      EXPECT_EQ(QDR.at(1).row(), size.col);
      EXPECT_EQ(QDR.at(1).col(), size.col);
      EXPECT_TRUE(QDR.at(1).diag());
      EXPECT_EQ(QDR.at(2).row(), size.col);
      EXPECT_EQ(QDR.at(2).col(), size.col);
    }
  }
}

TEST(Matrixz, LDQ_inplace_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    if (size.row <= size.col) {
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i, i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      Matrix<uni10_complex128> Id(size.row, size.row);
      Id.Identity();
      std::vector<Matrix<uni10_complex128>> LDQ(3);
      Ldq(M, LDQ.at(0), LDQ.at(1), LDQ.at(2), INPLACE);

      // Check if M equals to L * D *Q
      double eps1 = Norm(M - Dot(Dot(LDQ.at(0), LDQ.at(1)), LDQ.at(2)));
      // Check Q's orthogonality
      double eps2 = Norm(Id - Dot(LDQ.at(2), Dagger(LDQ.at(2))));

      // Check if L is lower triangular matrix
      bool isCorrect = true;
      for (size_t i = 0; i < LDQ.at(0).row() && isCorrect; ++i) {
        for (size_t j = i + 1; j < LDQ.at(0).col() && isCorrect; ++j) {
          if (LDQ.at(0)[i * LDQ.at(0).row() + j] != 0.0) isCorrect = false;
        }
      }

      EXPECT_LE(eps1, error);
      EXPECT_LE(eps2, error);
      EXPECT_TRUE(isCorrect);
      EXPECT_EQ(LDQ.at(0).row(), size.row);
      EXPECT_EQ(LDQ.at(0).col(), size.row);
      EXPECT_EQ(LDQ.at(1).row(), size.row);
      EXPECT_EQ(LDQ.at(1).col(), size.row);
      EXPECT_TRUE(LDQ.at(1).diag());
      EXPECT_EQ(LDQ.at(2).row(), size.row);
      EXPECT_EQ(LDQ.at(2).col(), size.col);
    }
  }
}

TEST(Matrixz, SVD_inplace_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    size_t sizeMin   = std::min(size.row, size.col);
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i)
      src[i] = std::complex<double> (i, i);

    Matrix<uni10_complex128> M(size.row, size.col, src);
    Matrix<uni10_complex128> Id(sizeMin, sizeMin);
    Id.Identity();
    std::vector<Matrix<uni10_complex128>> SVD(3);
    Svd(M, SVD.at(0), SVD.at(1), SVD.at(2), INPLACE);

    // Check if M equals to U * S * V^T
    double eps1 = Norm(M - Dot(Dot(SVD.at(0), SVD.at(1)), SVD.at(2)));
    // Check U's orthogonality
    double eps2 = SVD.at(0).row() < SVD.at(0).col() ?
                  Norm(Id - Dot(SVD.at(0), Dagger(SVD.at(0)))):
                  Norm(Id - Dot(Dagger(SVD.at(0)), SVD.at(0)));
    // Check V's orthogonality
    double eps3 = SVD.at(2).row() < SVD.at(2).col() ?
                  Norm(Id - Dot(SVD.at(2), Dagger(SVD.at(2)))):
                  Norm(Id - Dot(Dagger(SVD.at(2)), SVD.at(2)));

    EXPECT_LE(eps1, error);
    EXPECT_LE(eps2, error);
    EXPECT_LE(eps3, error);
    EXPECT_EQ(SVD.at(0).row(), size.row);
    EXPECT_EQ(SVD.at(0).col(), sizeMin);
    EXPECT_EQ(SVD.at(1).row(), sizeMin);
    EXPECT_EQ(SVD.at(1).col(), sizeMin);
    EXPECT_TRUE(SVD.at(1).diag());
    EXPECT_EQ(SVD.at(2).row(), sizeMin);
    EXPECT_EQ(SVD.at(2).col(), size.col);
  }
}

TEST(Matrixz, SDD_inplace_notDiag) {
  for (auto size : sizes) {
    size_t nelem = size.row * size.col;
    size_t sizeMin   = std::min(size.row, size.col);
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i)
      src[i] = std::complex<double> (i, i);

    Matrix<uni10_complex128> M(size.row, size.col, src);
    Matrix<uni10_complex128> Id(sizeMin, sizeMin);
    Id.Identity();
    std::vector<Matrix<uni10_complex128>> SDD(3);
    Sdd(M, SDD.at(0), SDD.at(1), SDD.at(2), INPLACE);

    // Check if M equals to U * S * V^T
    double eps1 = Norm(M - Dot(Dot(SDD.at(0), SDD.at(1)), SDD.at(2)));
    // Check U's orthogonality
    double eps2 = SDD.at(0).row() < SDD.at(0).col() ?
                  Norm(Id - Dot(SDD.at(0), Dagger(SDD.at(0)))):
                  Norm(Id - Dot(Dagger(SDD.at(0)), SDD.at(0)));
    // Check V's orthogonality
    double eps3 = SDD.at(2).row() < SDD.at(2).col() ?
                  Norm(Id - Dot(SDD.at(2), Dagger(SDD.at(2)))):
                  Norm(Id - Dot(Dagger(SDD.at(2)), SDD.at(2)));

    EXPECT_LE(eps1, error);
    EXPECT_LE(eps2, error);
    EXPECT_LE(eps3, error);
    EXPECT_EQ(SDD.at(0).row(), size.row);
    EXPECT_EQ(SDD.at(0).col(), sizeMin);
    EXPECT_EQ(SDD.at(1).row(), sizeMin);
    EXPECT_EQ(SDD.at(1).col(), sizeMin);
    EXPECT_TRUE(SDD.at(1).diag());
    EXPECT_EQ(SDD.at(2).row(), sizeMin);
    EXPECT_EQ(SDD.at(2).col(), size.col);
  }
}

TEST(Matrixz, Eig_inplace_notDiag) {
  for (auto size : sizes) {
    if (size.row == size.col) {
      size_t nelem = size.row * size.col;
      std::complex<double> src[nelem];

      for (size_t i = 0; i < nelem; ++i)
        src[i] = std::complex<double> (i, i);

      Matrix<uni10_complex128> M(size.row, size.col, src);
      std::vector<Matrix<uni10_complex128>> EIG(2);
      Eig(M, EIG.at(0), EIG.at(1), INPLACE);

      // Check if M equals to U * S * V^T
      double eps = Norm(M - Dot(Dot(Inverse(EIG.at(1)), EIG.at(0)), EIG.at(1)));

      EXPECT_LE(eps, error);
      EXPECT_EQ(EIG.at(0).row(), size.row);
      EXPECT_EQ(EIG.at(0).col(), size.col);
      EXPECT_TRUE(EIG.at(0).diag());
      EXPECT_EQ(EIG.at(1).row(), size.row);
      EXPECT_EQ(EIG.at(1).col(), size.col);
    }
  }
}

TEST(Matrixz, Transpose_inplaceNew_notDiag) {
  for (auto size : sizes){
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i)
      src[i] = std::complex<double> (i, i);

    Matrix<uni10_complex128> M(size.row, size.col, src);
    Matrix<uni10_complex128> T;
    Transpose(T, M, INPLACE);

    EXPECT_EQ(T.row(), size.col);
    EXPECT_EQ(T.col(), size.row);
    for (size_t i = 0 ; i < size.col; ++i){
      for (size_t j = 0 ; j < size.row; ++j){
        EXPECT_EQ(T[i * size.row + j], M[i + j * size.col]);
      }
    }
  }
}

TEST(Matrixz, Dagger_inplaceNew_notDiag) {
  for (auto size : sizes){
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i)
      src[i] = std::complex<double> (i, i);

    Matrix<uni10_complex128> M(size.row, size.col, src);
    Matrix<uni10_complex128> D;
    Dagger(D, M, INPLACE);

    EXPECT_EQ(D.row(), size.col);
    EXPECT_EQ(D.col(), size.row);
    for (size_t i = 0 ; i < size.col; ++i){
      for (size_t j = 0 ; j < size.row; ++j){
        EXPECT_EQ(D[i * size.row + j], std::conj(M[i + j * size.col]));
      }
    }
  }
}

TEST(Matrixz, Dagger_inplace_notDiag) {
  for (auto size : sizes){
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i)
      src[i] = std::complex<double> (i, i);

    Matrix<uni10_complex128> M(size.row, size.col, src);
    Matrix<uni10_complex128> D = M;
    Dagger(D, INPLACE);

    EXPECT_EQ(D.row(), size.col);
    EXPECT_EQ(D.col(), size.row);
    for (size_t i = 0 ; i < size.col; ++i){
      for (size_t j = 0 ; j < size.row; ++j){
        EXPECT_EQ(D[i * size.row + j], std::conj(M[i + j * size.col]));
      }
    }
  }
}


TEST(Matrixz, Conj_inplace_notDiag) {
  for (auto size : sizes){
    size_t nelem = size.row * size.col;
    std::complex<double> src[nelem];

    for (size_t i = 0; i < nelem; ++i)
      src[i] = std::complex<double> (i, i);

    Matrix<uni10_complex128> M(size.row, size.col, src);
    Matrix<uni10_complex128> C = M;
    Conj(C, INPLACE);

    EXPECT_EQ(C.row(), size.row);
    EXPECT_EQ(C.col(), size.col);
    for (size_t i = 0 ; i < nelem; ++i){
      EXPECT_EQ(C[i], std::conj(M[i]));
    }
  }
}


#endif
